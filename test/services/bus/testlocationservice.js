// var assert = require('assert'),
var chai = require("chai"),
    expect = chai.expect,
    // chaiAsPromised = require("chai-as-promised"),
    LocationService = require('../../../app/services/bus/LocationService');

chai.should();

describe('LocationService#getSourceCityId', function() {

    it('respond in promise style', function() {
        var locationservice = new LocationService();
        var resultPromise = locationservice.getSourceCityId("bangalore");
        resultPromise.should.have.property("then");
        resultPromise.should.have.property("fail");
    });

//When testing promises it is must to call the service inside it, and it is must to pass done as parameter to it.
    it('respond with matching records', function(done) {        
        var locationservice = new LocationService();
        var resultPromise = locationservice.getSourceCityId("bangalore");
        resultPromise.done(function(res) {
            res.should.be.a('string')    
            res.should.be.equal('3')       
            done();
        });
    });

    it('invalid source city', function(done) {        
        var locationservice = new LocationService();
        var resultPromise = locationservice.getSourceCityId("goa");
        resultPromise.done(function(res) {
            res.should.be.a('number')    
            res.should.be.equal(-1)       
            done();
        });
    });

});

describe('LocationService#getCityId', function() {

    it('respond in promise style', function() {
        var locationservice = new LocationService();
        var resultPromise = locationservice.getCityId("chennai");
        resultPromise.should.have.property("then");
        resultPromise.should.have.property("fail");
    });

//When testing promises it is must to call the service inside it, and it is must to pass done as parameter to it.
    it('respond with matching records', function(done) {        
        var locationservice = new LocationService();
        var resultPromise = locationservice.getCityId("chennai");
        resultPromise.done(function(res) {
            res.should.be.a('string')    
            res.should.be.equal('1')       
            done();
        });
    });

});

describe('LocationService#isSourceDestinationPairValid', function() {

    it('respond in promise style', function() {
        var locationservice = new LocationService();
        var resultPromise = locationservice.isSourceDestinationPairValid("chennai","bangalore");
        resultPromise.should.have.property("then");
        resultPromise.should.have.property("fail");
    });

//When testing promises it is must to call the service inside it, and it is must to pass done as parameter to it.
    it('respond with matching records since it s valid route', function(done) {        
        var locationservice = new LocationService();
        var resultPromise = locationservice.isSourceDestinationPairValid("chennai","bangalore");
        resultPromise.done(function(res) {
            res.should.be.a('boolean')    
            res.should.be.equal(true)       
            done();
        });
    });

    it('respond false since it is invalid route', function(done) {        
        var locationservice = new LocationService();
        var resultPromise = locationservice.isSourceDestinationPairValid("goa","bangalore");
        resultPromise.done(function(res) {
            res.should.be.a('boolean')    
            res.should.be.equal(false)       
            done();
        });
    });

});
