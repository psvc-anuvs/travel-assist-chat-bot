// var assert = require('assert'),
var chai = require("chai"),
    expect = chai.expect,
    // chaiAsPromised = require("chai-as-promised"),
    BookingService = require('../../../app/services/bus/BookingService');

chai.should();

describe('BookingService', function() {

    it('respond in promise style', function() {
        var bookingservice = new BookingService();
        var resultPromise = bookingservice.getAvailableTrips("bangalore", "chennai", "2016-07-03");
        resultPromise.should.have.property("then");
        resultPromise.should.have.property("fail");
    });

//When testing promises it is must to call the service inside it, and it is must to pass done as parameter to it.
    it('respond with matching records', function(done) {        
        var bookingservice = new BookingService();
        var resultPromise = bookingservice.getAvailableTrips("bangalore", "chennai", "2016-07-03");
        resultPromise.done(function(res) {
            res.should.be.a('array');
            res.should.have.length(2);
            done();
        });
    });

});
