// var assert = require('assert'),
var chai = require("chai"),
    expect = chai.expect,
    // chaiAsPromised = require("chai-as-promised"),
    IbiboBusService = require('../../../app/services/bus/IbiboBusService');

chai.should();

describe('IbiboBusService#getAvailableTrips', function() {

    it('respond in promise style', function() {
        var busService = new IbiboBusService();
        var resultPromise = busService.getAvailableTrips("bangalore", "chennai", "20161027");
        resultPromise.should.have.property("then");
        resultPromise.should.have.property("catch");
    });

//When testing promises it is must to call the service inside it, and it is must to pass done as parameter to it.
    it('respond with matching records', function(done) {  
    this.timeout(5000);      
        var busService = new IbiboBusService();
        var resultPromise = busService.getAvailableTrips("bangalore", "hyderabad", "20161027");
        resultPromise.done(function(res) {
            // console.log(res.onwardflights.length);
            res.onwardflights.should.be.a('array');            
            done();
        });
    });

});

describe('IbiboBusService#getSeatLayout', function() {

    it('respond in promise style', function() {
        var busService = new IbiboBusService();
        var resultPromise = busService.getSeatLayout("vJ52KC0ymd0635WCEN-_DzZh6c0Fl5FJMpnf0aBU9mQTyu0C3iRO3mPRkbeN8Cm8Kh_m2OIwMLNEZg==");
        resultPromise.should.have.property("then");
        resultPromise.should.have.property("catch");
    });

//When testing promises it is must to call the service inside it, and it is must to pass done as parameter to it.
    it('respond with matching records', function(done) {  
    this.timeout(5000);      
        var busService = new IbiboBusService();
        var resultPromise = busService.getSeatLayout("vJ52KC0ymd0635WCEN-_DzZh6c0Fl5FJMpnf0aBU9mQTyu0C3iRO3mPRkbeN8Cm8Kh_m2OIwMLNEZg==");
        resultPromise.done(function(res) {
            // console.log(res.onwardSeats.length);
            res.onwardSeats.should.be.a('array');            
            done();
        });
    });

});
