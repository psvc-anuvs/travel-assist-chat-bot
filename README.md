# Travel Chat Bot
	A chat bot which takes care of user's travel needs, such as, booking/cancelling bus, hotel, car.

* Planned features for initial launch:- 
	1. Check bus availability
	2. Book bus
	3. Get bus notifications such as tickets are filling fast, holiday booking, etc

* Additional Planned Features
	1. Book car before starting the bus journey.
	2. Book hotel

* To start developement,
	1. Run 'npm install && bower install' only once.
	2. Run grunt
	3. Run 'lt --subdomain bbot --port 3000' in separate window -- Local Tunnel (npm install -g lt)

* To debug,
	1. Install devtool(npm install -g devtool) globally, and make sure to use node 6.4 version.	
	2. Run devtool app.js, it should open an electron window, through which we can debug, this works same way as chrome dev tools.

* General Information,
	1. No need to start mongo db separately, running grunt will start both app and mongodb
	2. Live reload is there in grunt, so whenever you make changes, try running local tunnel again.	