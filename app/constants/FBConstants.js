/* 
 * A list of constants which is used in facebook related messages or requests
 */
module.exports = Object.freeze({
     Router: require('express').Router,
     FB_URL: 'https://graph.facebook.com/v2.6/',
     FB_MESSENGER_URL: 'https://graph.facebook.com/v2.6/me/messages',
     FB_SUBSCRIPTION_URL: 'https://graph.facebook.com/v2.6/me/subscribed_apps',
     FB_THREAD_SETTINGS_URL: 'https://graph.facebook.com/v2.6/me/thread_settings',

     BUTTON_TYPE: {
        POSTBACK: 'postback',
        WEB_URL: 'web_url',
        ACCOUNT_LINK: 'account_link',
        SHARE: 'element_share',
        CALL:'phone_number',
        BUY: 'payment'
    },

     TEMPLATE_TYPE: {
        BUTTON: 'button',
        RECEIPT: 'receipt',
        GENERIC: 'generic'
    },

     ATTACHMENT_TYPE: {
        IMAGE: 'image',
        FILE: 'file',
        VIDEO: 'video',
        AUDIO: 'audio',
        TEMPLATE: 'template'
    },

     NOTIFICATION_TYPE: {
        REGULAR: 'REGULAR',
        SILENT_PUSH: 'SILENT_PUSH',
        NO_PUSH: 'NO_PUSH'
    },

     CONTENT_TYPE: {
        TEXT: 'text',
        LOCATION: 'location'
    },

     ACTION_TYPES: {
        MARK_SEEN: 'mark_seen',
        TYPING_ON: 'typing_on',
        TYPING_OFF: 'typing_off',
    },

     WEBVIEW_HEIGHT_RATIO: {
        FULL: 'full',
        TALL: 'tall',
        COMPACT: 'compact'
    }
});
