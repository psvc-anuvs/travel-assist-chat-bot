/* 
 * A gloabl list of constants which is used in message conversations as post backs.
 * Post Back: Unique message send by messenger when ever an user performs an action like,
 * clicking buttons in the messages, clicking getting started button, clicking menu items etc.
 */
module.exports = Object.freeze({
    PB_NEW_USER_JOINED_0: 'PB_NEW_USER_JOINED_0',
    PB_WELCOME_NEW_USER_1: 'PB_WELCOME_NEW_USER_1',
    PB_MENU_1: 'PB_BOOK_BUS_TICKET_1',
    PB_MENU_2: 'PB_CANCEL_BUS_TICKET_2',
    PB_MENU_3: 'PB_SETTINGS_3',
    PB_MENU_4: 'PB_HELP_4'
});
