var emoji = require('node-emoji');

/* 
 * A gloabl list of constants which is used in controller.hear.
 */
module.exports = Object.freeze({
    MSG_GREETING: 'Hi {{user_first_name}}, I am a bus('+
    	emoji.get('bus') +') booking bot(' +
        emoji.get('woman') + ').  Need help, in booking bus ticket(' +
        emoji.get('ticket') + ') for you and your family(' +
        emoji.get('family') + ') , ping me.',
    MSG_MENU_1: 'Book Bus ' + emoji.get('bus'),
    MSG_MENU_2: 'Upcoming Trips',
    MSG_MENU_3: 'Settings',
    MSG_MENU_4: emoji.get('question') + 'Help',
    MSG_HI: new RegExp(/^(hi|halo|helo|hello|hey)/i),
    MSG_CANCEL: new RegExp(/^(cancel|exit|stop)/i),
    MSG_BOOK_BUS: new RegExp(/^(book bus|bk bus|book a bus)/i),
    MSG_SET_BOOKING_ALERT: new RegExp(/^(bus alerts|bkng alerts|booking alerts|set notification|notify me)/i)
});
