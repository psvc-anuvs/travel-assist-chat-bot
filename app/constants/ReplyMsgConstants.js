var emoji = require('node-emoji');

/* 
 * A gloabl list of constants which is used in bot replies.
 */
module.exports = Object.freeze({
    RP_HI_MSG: 'Hi, How can i help you ' + emoji.get('question'),
    RP_BOOK_BUS: 'Book a bus ' + emoji.get('bus'),
    RP_SET_BOOKING_ALERTS: 'Set Notification ' + emoji.get('alarm_clock'),
    RP_CANCEL_TEXT: ' Type \'Cancel\', to stop this conversation anytime you wish.',
    RP_BOOK_BUS_START_TEXT : [
    'ok. Let me help you book a bus. ',
    'Let me assist you choose the right bus. ',
    'I will help you in finding suitable bus.' 
    ]
});
