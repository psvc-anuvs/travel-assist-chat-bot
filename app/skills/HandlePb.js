var pbConstants = require('../constants/PbConstants'),
    FBUtils = require('../utils/FBUtils');

var HandlePostBacks = function(controller, bot, app) {
    var fbutils = new FBUtils();

    controller.on('facebook_postback', function(bot, message) {

        // create new user
        if (message.payload == pbConstants.PB_NEW_USER_JOINED_0) {
            var options = {};
            options.userId = message.sender.id;
            options.ts = message.timestamp;
            fbutils.getUserProfile(options)
                .then(function(userProfile) {
                    controller.storage.users.save(profile);
                })
                .catch(function(err) {
                    controller.log('Error in creating user profile ' + err);
                });
        } else if (message.payload == pbConstants.PB_WELCOME_NEW_USER_1) {
            bot.reply(message, "hello new user, how are you?");
        } else if (message.payload == pbConstants.PB_MENU_1) {
            bot.reply(message, "User clicked menu 1");
        } else if (message.payload == pbConstants.PB_MENU_2) {
            bot.reply(message, "User clicked menu 2");
        } else if (message.payload == pbConstants.PB_MENU_3) {
            bot.reply(message, "User clicked menu 3");
        } else if (message.payload == pbConstants.PB_MENU_4) {
            bot.reply(message, "User clicked menu 4");
        }

    });
}

module.exports = HandlePostBacks;
