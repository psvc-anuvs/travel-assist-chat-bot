var msgConstants = require('../constants/MsgConstants'),
    replyConstants = require('../constants/ReplyMsgConstants'),
    FBUtils = require('../utils/FBUtils');

var Welcome = function(controller, bot, app) {
    var fbUtils = new FBUtils();

    // user said hi, helo, halo, etc
    controller.hears([msgConstants.MSG_HI], 'message_received', function(bot, message) {

        var quickReply1 = fbUtils.createQuickReply(replyConstants.RP_BOOK_BUS, replyConstants.RP_BOOK_BUS);
        var quickReply2 = fbUtils.createQuickReply(replyConstants.RP_SET_BOOKING_ALERTS, replyConstants.RP_SET_BOOKING_ALERTS);
        var replyMsg = {
            text: replyConstants.RP_HI_MSG,
            quick_replies: [quickReply1, quickReply2]
        }
        bot.reply(message, replyMsg);
    });
}

module.exports = Welcome;
