var msgConstants = require('../constants/MsgConstants'),
    replyConstants = require('../constants/ReplyMsgConstants'),
    FBUtils = require('../utils/FBUtils'),
    CommonUtils = require('../utils/CommonUtils'),
    LocationService = require('../services/bus/LocationService'),
    _ = require('underscore');

var BookBus = function(controller, bot, app) {

    var fbutils = new FBUtils();
    var locationservice = new LocationService();
    var commonutils = new CommonUtils();

    var cancelCB = function(response, convo) {
        convo.say('If you decide to travel again. Let me know.');
        convo.next();
    }

    var sourceCityDefaultCB = function(response, convo) {
        controller.storage.buscities.getByName(response.text.toUpperCase())
            .then(function(res) {

                if (res == null) {
                    convo.say('I am sorry, i could not find any bus starting from \'' + response.text + '\'');
                    convo.repeat();
                } else {
                    // console.log('insied else loop');
                    convo.setVar('startingCity', response.text);
                    var user = convo.vars.user;
                    if (user) {
                        user = commonutils.addOrUpdateArray(user, 'freqSearchedCities', response.text);
                        controller.storage.users.save(user);
                    }
                    convo.say('ok,{{#vars.user}} {{vars.user.first_name}}{{/vars.user}} your starting city is \'{{vars.startingCity}}\'');
                    askDestinationCity(response, convo);
                }
            })
            .catch(function(err) {
                convo.say('I am sorry at this moment i could not help you.');
            });
        convo.next();
    }
    var askSourceCity = function(err, convo) {
        var msg = 'Travelling from ?';

        var quickReplies = fbutils.buildCitiesQuickReply(convo.vars.user);
        if (quickReplies.length > 0) {
            msg = {
                text: 'Travelling from ? if city is not shown in options please type.',
                quick_replies: quickReplies
            }
        }

        convo.ask(msg, [{
            pattern: msgConstants.MSG_CANCEL,
            callback: cancelCB
        }, {
            default: true,
            callback: sourceCityDefaultCB
        }]);
    };

    var destinationCityCB = function(response, convo) {
        controller.storage.buscities.getByName(response.text.toUpperCase())
            .then(function(res) {
                if (res == null) {
                    convo.say('\'' + response.text + '\' is not a valid city. Try another city.');
                    convo.repeat();
                } else {
                    var destinationCity = response.text;
                    convo.setVar('destinationCity', destinationCity);
                    var user = convo.vars.user;
                    if (user) {
                        user = commonutils.addOrUpdateArray(user, 'freqSearchedCities', destinationCity);
                        // convo.vars.user = user;
                        controller.storage.users.save(user);
                    }                    
                    convo.say('ok, you have decided to travel to \'{{vars.destinationCity}}\'');
                    askTravelDate(response, convo);
                }
            })
            .catch(function(err) {
                convo.say('I am sorry at this moment i could not help you.');
            });
        convo.next();
    }

    var askDestinationCity = function(response, convo) {
        var msg = 'Travelling to ?';

        var quickReplies = fbutils.buildCitiesQuickReply(convo.vars.user, convo.vars.startingCity);
        if (quickReplies.length > 0) {
            msg = {
                text: 'Travelling to ? if city is not shown in options please type.',
                quick_replies: quickReplies
            }
        }

        convo.ask(msg, [{
            pattern: msgConstants.MSG_CANCEL,
            callback: cancelCB
        }, {
            default: true,
            callback: destinationCityCB
        }]);
    };

    var askTravelDate = function(response, convo) {
        convo.ask('When do you want to travel ?', function(response, convo) {
            convo.setVar('travelDate', response.text);
            convo.sayFirst('Ok! You want to travel from {{vars.startingCity}} to {{vars.destinationCity}} on {{vars.travelDate}}. Let me find right bus for you.');
            convo.next();
        });
    };

    // user said book bus and messages relevant to this which has similar meaning
    controller.hears([msgConstants.MSG_BOOK_BUS], 'message_received', function(bot, message) {

        bot.startConversation(message, function(err, convo) {
            // // now set the conversation in motion...
            // convo.activate();   
            convo.sayFirst(_.sample(replyConstants.RP_BOOK_BUS_START_TEXT) + replyConstants.RP_CANCEL_TEXT);
            controller.storage.users.get(message.user)
                .then(function(res) {
                    convo.setVar('user', res);
                    // convo.activate();
                    askSourceCity(err, convo);
                }) //TODO: when bot.createConversation feature is available for fb bot remove this
                .catch(function(err) {
                    askSourceCity(err, convo);
                });

            convo.on('end', function(convo) {
                // console.log('inside conversation end');
                var data = {
                    startingCity: convo.vars.startingCity,
                    destinationCity: convo.vars.destinationCity,
                    travelDate: convo.vars.travelDate,
                    channel: message.channel
                }
                if (convo.status == 'completed') {
                    controller.trigger('123456', [bot, data]);
                } else {
                    controller.log('Error in conversation getAvail');
                }
            });

        });
    });

    controller.on('123456', function(bot, data) {
        // console.log('inside 123456 controller on method');
        // console.log(message);
        // console.log(data);
        var msg = {
            channel: data.channel,
            text: 'Hello Say'
        }
        bot.say(msg);
    });
}

module.exports = BookBus;
