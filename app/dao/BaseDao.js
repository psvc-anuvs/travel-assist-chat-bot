var BaseDao = function(collection) {
    this.collection = collection;
    // this.unwrapFromList = function(cb) {
    //     return function(err, data) {
    //         if (err) return cb(err)
    //         cb(null, data)
    //     }
    // }
}

BaseDao.prototype.get = function(id) {
    // this.collection.findOne({ id: id }, this.unwrapFromList(cb));
    return this.collection.findOne({ id: id });
};

BaseDao.prototype.getByName = function(name) {    
    return this.collection.findOne({ name: name });
};

BaseDao.prototype.save = function(data) {
    return this.collection.findOneAndUpdate({
        id: data.id
    }, data, {
        upsert: true,
        new: true
    });
};

BaseDao.prototype.all = function() {
    return this.collection.find({});
};

BaseDao.prototype.insert = function(data) {
    return this.collection.insert(data);
};

BaseDao.prototype.removeAll = function() {
    return this.collection.remove({});
};

module.exports = BaseDao;
