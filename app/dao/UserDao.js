var BaseDao = require('../dao/BaseDao')

var UserDao = function(collection) {
    BaseDao.call(this, collection)
}

UserDao.prototype = Object.create(BaseDao.prototype);

UserDao.prototype.constructor = UserDao;

UserDao.prototype.find_or_create = function(data) {
    //Since 'this' object has to be referenced inside then, so assigining it to currentObj
    var currentObj = this;
    return currentObj.collection
        .findOne({
            id: data.id
        })
        .then(function(user) {
            if (!user) {
                currentObj.collection
                    .findOneAndUpdate({
                        id: data.id
                    }, data, {
                        upsert: true,
                        new: true
                    })
                    .then(function(user) {
                        return user;
                    });
            } else {
                return user;
            }
        })        
        .catch(function(err) {
            console.log('Err in creating user ' + err);
        });
}

module.exports = UserDao;
