var _ = require('underscore');

/* 
 * A utility for all the commonly performed operations
 */
var CommonUtils = function() {}

/* addOrUpdateArray
 * This method checks whether the passed array property is present in json object or not. If present it adds 
 * value otherewise it creates the property and adds.
 * @param  {Object} jsonObj 
 * @param  {String} arrayProperty
 * @param  {String} value 
 * @return {Object} modifiedJsonObject
 */
CommonUtils.prototype.addOrUpdateArray = function(jsonObj, arrayProperty, value) {
	var array = [];
	if(jsonObj.hasOwnProperty(arrayProperty)){
		array = jsonObj[arrayProperty];
	} 
	array.push(value);
	jsonObj[arrayProperty] = _.uniq(array);
	return jsonObj;
};

module.exports = CommonUtils;