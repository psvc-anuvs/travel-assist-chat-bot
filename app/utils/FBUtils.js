var fbConstants = require('../constants/FBConstants'),
    pbConstants = require('../constants/PbConstants'),
    msgConstants = require('../constants/MsgConstants'),
    request = require('request'),
    _ = require('underscore'),
    q = require('q');

/* 
 * A utility for facebook bot, which facilitates a way to create buttons or templates for messenger bot
 * or subscribing to page, adding menu etc.
 */
var FBUtils = function() {}

/* getUserProfile
 * This method calls facebook's graph api and gets the data related the user.
 * @param  {Object} options - it is a simple json object which contains user Id and the message timestamp.
 * @return {Object} User profile, if the api call is success, then returns with first_name, last_name etc. 
 *                  Otherwise, it returns an profile just with the id and timestamp which is passed.
 */
FBUtils.prototype.getUserProfile = function(options) {
    var defer = q.defer();
    const USER_URL = fbConstants.FB_URL + options.userId;
    request.get({
        url: USER_URL,
        qs: {
            fields: 'first_name,last_name,profile_pic,locale,timezone,gender',
            access_token: process.env.FACEBOOK_PAGE_TOKEN
        },
        json: true

    }, function(err, res, body) {
        if (err) {
            defer.reject(err);
        } else {
            if (body && body.first_name) {
                body.id = options.userId;
                body.created_at = options.ts;
                defer.resolve(body);
            } else {
                var profile = {};
                profile.id = options.userId;
                profile.created_at = options.ts;
                defer.resolve(profile);
            }
        }
    });
    return defer.promise;
}

/* subscribePage
 * This method calls facebook's subscribe page api to receive message events.
 * @param  {Object} callback - A function, which has to be called once we receive response from api.
 */
FBUtils.prototype.subscribePage = function(callback) {

    request.post({
        url: fbConstants.FB_SUBSCRIPTION_URL,
        json: true,
        qs: {
            access_token: process.env.FACEBOOK_PAGE_TOKEN
        }
    }, function(err, res, body) {
        if (callback) {
            callback(err, body);
        }
    });
}

/* setPersistentMenu
 * This method calls facebook's thread settings api to add persistant menu to our bot.
 * Persistant Menu: The Persistent Menu is a menu that is always available to the user.
 * @param  {Object} options - A simple json which contains an array of buttons which has to be added as menu.
 * @param  {Object} callback - A function, which has to be called once we receive response from api.
 */
FBUtils.prototype.setPersistentMenu = function(options, callback) {

    request.post({
        url: fbConstants.FB_THREAD_SETTINGS_URL,
        json: true,
        qs: {
            access_token: process.env.FACEBOOK_PAGE_TOKEN
        },
        body: {
            setting_type: 'call_to_actions',
            thread_state: 'existing_thread',
            call_to_actions: options.buttons
        }

    }, function(err, res, body) {
        if (callback) {
            callback(err, body);
        }
    });
}

/* setGreetingText
 * This method calls facebook's thread settings api to add greeting text which appears for the first user in welcome page.
 * @param  {Object} callback - A function, which has to be called once we receive response from api.
 */
FBUtils.prototype.setGreetingText = function(callback) {

    request.post({
        url: fbConstants.FB_THREAD_SETTINGS_URL,
        json: true,
        qs: {
            access_token: process.env.FACEBOOK_PAGE_TOKEN
        },
        body: {
            setting_type: 'greeting',
            greeting: {
                text: msgConstants.MSG_GREETING
            }
        }

    }, function(err, res, body) {
        if (callback) {
            callback(err, body);
        }
    });
}

/* setGettingStartedPayLoad
 * This method calls facebook's thread settings api to add payload to 'Getting Started' button which appears in welcome page.
 * @param  {Object} callback - A function, which has to be called once we receive response from api.
 */
FBUtils.prototype.setGettingStartedPayLoad = function(callback) {

    request.post({
        url: fbConstants.FB_THREAD_SETTINGS_URL,
        json: true,
        qs: {
            access_token: process.env.FACEBOOK_PAGE_TOKEN
        },
        body: {
            setting_type: "call_to_actions",
            thread_state: "new_thread",
            call_to_actions: [{
                payload: pbConstants.PB_NEW_USER_JOINED_0,
                payload: pbConstants.PB_WELCOME_NEW_USER_1
            }]
        }

    }, function(err, res, body) {
        if (callback) {
            callback(err, body);
        }
    });
}

/* setWhiteList
 * This method calls facebook's thread settings api to whitelist our domain.
 * We have to whitelist our domain, only then webviews will work.
 * @param  {Object} callback - A function, which has to be called once we receive response from api.
 */
FBUtils.prototype.setWhiteList = function(callback) {

    request.post({
        url: fbConstants.FB_THREAD_SETTINGS_URL,
        json: true,
        qs: {
            access_token: process.env.FACEBOOK_PAGE_TOKEN
        },
        body: {
            setting_type: 'domain_whitelisting',
            whitelisted_domains: [process.env.APP_DOMAIN],
            domain_action_type: 'add'
        }

    }, function(err, res, body) {
        if (callback) {
            callback(err, body);
        }
    });
}

/* createWebURLButton
 * This method creates a button which acts as a link, when user taps, it will open a webpage in-app browser.
 * @param  {String} title - A title for the button, which is the actual text displayed in the button.
 * @param  {String} url - This URL is opened in a mobile browser when the button is tapped
 * @param  {String} heightRatio - Height of the webview possible values are full, tall and compact.
 * @param  {String} supportExtension - Must be true if using messenger extension ex: webview.
 * @param  {String} fallbackURL - URL to use on clients that don't support Messenger Extensions. 
                                  If this is not defined, above url parameter value will be used as the fallback.
 * @return {Object} button
 */
FBUtils.prototype.createWebURLButton = function(title, url, heightRatio, supportExtension, fallbackURL) {
    var button = {
        type: fbConstants.BUTTON_TYPE.WEB_URL,
        title: title,
        url: url
    };
    if (heightRatio) {
        button.webview_height_ratio = heightRatio;
    }
    if (supportExtension) {
        button.messenger_extensions = supportExtension;
    }
    if (fallbackURL) {
        button.fallback_url = fallbackURL;
    }
    return button;
}

FBUtils.prototype.createAccountLinkButton = function(url) {
    return {
        type: fbConstants.BUTTON_TYPE.ACCOUNT_LINK,
        url: url
    };
}

/* createWebURLButton
 * This method creates a share button which enables people to share message bubbles with their contacts 
 * using a native share dialog in Messenger.
 * @return {Object} button
 */
FBUtils.prototype.createShareButton = function() {
    return {
        type: fbConstants.BUTTON_TYPE.SHARE,
    };
}

/* createCallButton
 * This method creates a call button which can be used to initiate a phone call.
 * @param  {String} title - A title for the button, which is the actual text displayed in the button.
 * @param  {String} payload - Phone number.
 * @return {Object} button
 */
FBUtils.prototype.createCallButton = function(title, payload) {
    return {
        type: fbConstants.BUTTON_TYPE.CALL,
        title: title,
        payload: payload
    };
}

/* createPostbackButton
 * This method creates a post back button which sends people's action to the bot.
 * @param  {String} title - A title for the button, which is the actual text displayed in the button.
 * @param  {String} payload - A Unique data, which has to be send back when user taps the button.
 * @return {Object} button
 */
FBUtils.prototype.createPostbackButton = function(title, payload) {
    return {
        type: fbConstants.BUTTON_TYPE.POSTBACK,
        title: title,
        payload: payload
    };
}

/* createQuickReply
 * This method creates a quick reply button which appears on top of composer and disappers after user taps.
 * @param  {String} title - A title for the button, which is the actual text displayed in the button.
 * @param  {String} payload - A Unique data, which has to be send back when user taps the button.
 * @param  {String} imageURL - URL of the image which has to be displayed with quick reply.
 * @return {Object} button
 */
FBUtils.prototype.createQuickReply = function(title, payload, imageURL) {
    let reply = {
        content_type: fbConstants.CONTENT_TYPE.TEXT,
        title: title,
        payload: payload,
    };
    if (imageURL) {
        reply.image_url = imageURL;
    }
    return reply;
}

/* createShareLocation
 * This method creates a send loaction button which helps user share his location.
 * @return {Object} button
 */
FBUtils.prototype.createShareLocation = function() {
    return {
        content_type: fbConstants.CONTENT_TYPE.LOCATION,
    };
}

/* createCard
 * This method creates a card object which will be used with generic template to create horizontal card.
 * @param  {String} title - Bubble title.
 * @param  {String} subTitle - Bubble subtitle (Optional).
 * @param  {String} itemUrl - URL that is opened when bubble is tapped (Optional).
 * @param  {String} imageURL - Bubble image (Optional).
 * @param  {Array} buttons - Set of buttons that appear as call-to-actions (Optional).
 * @return {Object} element
 */
FBUtils.prototype.createCard = function(title, subTitle, itemUrl, imageURL, buttons) {
    let card = {
        title: title,
        item_url: itemUrl,
        image_url: image_url,
        subtitle: subtitle,
        buttons: buttons
    };
    if (itemUrl) {
        card.item_url = itemUrl;
    }
    if (imageURL) {
        card.image_url = imageURL;
    }
    if (subtitle) {
        card.subtitle = subtitle;
    }
    if (buttons) {
        card.buttons = buttons;
    }
    return card;
}

/* createButtonTemplate
 * This method creates a template message with an array of buttons with text passed to it.
 * @param  {String} text - Text that appears in main body
 * @param  {Array} buttons - List of buttons which has to be appear in the button template.
 * @return {Object} payload
 */
FBUtils.prototype.createButtonTemplate = function(text, buttons) {
    if (!Array.isArray(buttons)) {
        buttons = [buttons];
    }
    return {
        template_type: fbConstants.TEMPLATE_TYPE.BUTTON,
        text: text,
        buttons: buttons
    };
}

/* createGenericTemplate
 * This method creates a generic template message with an array of buttons with text which is horizontally scrollable content.
 * @param  {Array} elements - List of elements which usually contains image, buttons and text.
 * @return {Object} payload
 */
FBUtils.prototype.createGenericTemplate = function(elements) {
    if (!Array.isArray(elements)) {
        elements = [elements];
    }
    return {
        template_type: fbConstants.TEMPLATE_TYPE.GENERIC,
        elements: elements
    };
}

/* buildCitiesQuickReply
 * This method creates a list of quick replies based on user's frequently searched and traveled cities.
 * @param  {Object} user - A title for the button, which is the actual text displayed in the button.
 * @param  {String} city - If there is a value, then remove it from quick replies, useful in case of building dest cities.
 * @return {Object} citiesQuickReply
 */
FBUtils.prototype.buildCitiesQuickReply = function(user, optionalCity) {
    let quickReplies = [];
    if (user) {
        let freqCities = user.freqSearchedCities;

        if (freqCities) {
            // console.log(optionalCity);
            if (optionalCity) {
                // console.log(freqCities);
                freqCities = _.without(freqCities, optionalCity);
                // console.log(freqCities);
            }

            let freqCitiesLen = freqCities.length;
            if (freqCitiesLen >= 9) {
                freqCitiesLen = 9;
            }            
            for (i = 0; i < freqCitiesLen; i++) {
                let city = freqCities[i];
                quickReplies.push(this.createQuickReply(city, city));
            }
        }

    }

    return quickReplies;
}

module.exports = FBUtils;
