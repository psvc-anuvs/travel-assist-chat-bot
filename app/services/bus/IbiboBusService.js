var q = require('q'),
    request = require('request');

/* IbiboBusService
 * A list of functions which helps in checking availability and getting seat layout from goibibo..
 */
var IbiboBusService = function() {
    this.domainUrl = "http://developer.goibibo.com/api/";
    this.app_id = "7a4b5607";
    this.app_key = "f22b85320a16a291f243fae456cc6728";
}

/* getAvailableTrips
 * Takes source city and destination city and date of journey and checks the availability of trips in bus GDS.
 * @param  {String} sourceCityName Ex: Bangalore
 * @param  {String} destinationCityName Ex: Chennai
 * @param  {String} dateOfJourney Ex: 20161114 YYYYMMDD
 * @param  {String} returnJourney Ex: 20161114 YYYYMMDD it's optional
 * @return {Object} A list of trips which contains information reg travels name, seat availability etc
 */
IbiboBusService.prototype.getAvailableTrips = function(sourceCityName, destinationCityName, dateOfJourney) {
    var defer = q.defer();
    const URL = this.domainUrl + 'bus/search/';
    request.get({
        url: URL,
        qs: {
            'app_id': this.app_id,
            'app_key': this.app_key,
            'source': sourceCityName,
            'destination': destinationCityName,
            'dateofdeparture': dateOfJourney
        }
    }, function(error, response, body) {
        if (error) {
            defer.reject(error);
        } else if (!error && response.statusCode == 200) {            
            // console.log(Object.prototype.toString.call(body));            
            var parsedObj = JSON.parse(body);
            defer.resolve(parsedObj.data);
        } else { // TODO: not sure how to cover this in mocha test
            defer.resolve('Bad Response');
        }
    });

    return defer.promise;
}

/* bookTicket
 * Takes tripId and inventoryItems and books ticket in bus GDS.
 * @param  {String} tripId contains unique Id for the trip like "0001".
 * @param  {Object} inventoryItems contains seat, passenger and fare details.
 * @return {String} ticketNumber - unique Ticket Number
 */
IbiboBusService.prototype.bookTicket = function(tripId, inventoryItems) {
    return "12345";
}

/* getSeatLayout
 * Takes skey, an unique identifier to identify the bus, looks like session Id.
 * @param  {String} skey contains unique Id for the bus like "vJ52KC0ymd0635WJDtm6HEkW6M0Fn5ZONJre1aNO9mQXyvQCKhpB3WHkj7KEAi5yKCDr0".
 * @return {Object} seatLayout
 */
IbiboBusService.prototype.getSeatLayout = function(skey) {
    var defer = q.defer();
    const URL = this.domainUrl + 'bus/seatmap/';
    request.get({
        url: URL,
        qs: {
            'app_id': this.app_id,
            'app_key': this.app_key,
            'skey': skey
        }
    }, function(error, response, body) {
       if (error) {
            defer.reject(error);
        } else if (!error && response.statusCode == 200) {                          
            var parsedObj = JSON.parse(body);
            defer.resolve(parsedObj.data);
        } else {
            defer.resolve('Bad Response');
        }
    });
    return defer.promise;
}

module.exports = IbiboBusService;
