var q = require('q'),
    fs = require('fs');

/* BookingService
 * A list of functions which helps in checking availability and book ticket
 */
var BookingService = function() {}

/* getAvailableTrips
 * Takes source city and destination city and date of journey and checks the availability of trips in bus GDS.
 * @param  {String} sourceCityName Ex: Bangalore
 * @param  {String} destinationCityName Ex: Chennai
 * @param  {String} dateOfJourney Ex: 2012-07-16
 * @return {Object} A list of trips which contains information reg travels name, seat availability etc
 */
BookingService.prototype.getAvailableTrips = function(sourceCityName, destinationCityName, dateOfJourney) {
    var defer = q.defer();
    fs.readFile('./app/services/bus/mockdata/availabletrips.json', 'utf8', function(err, data) {
        // console.log("inside read file");
        if (err) {
            // console.log(err);
            defer.reject(err);
        } else {
            var json = JSON.parse(data);
            defer.resolve(json[sourceCityName + "-" + destinationCityName]);
        }
    });
    return defer.promise;
}

/* bookTicket
 * Takes tripId and inventoryItems and books ticket in bus GDS.
 * @param  {String} tripId contains unique Id for the trip like "0001".
 * @param  {Object} inventoryItems contains seat, passenger and fare details.
 * @return {String} ticketNumber - unique Ticket Number
 */
BookingService.prototype.bookTicket = function(tripId, inventoryItems) {
    return "12345";
}

module.exports = BookingService;
