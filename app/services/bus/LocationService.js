var q = require('q'),
    fs = require('fs');

/* LocationService
 * A list of functions which holds details of source and destination cities
 */
var LocationService = function() {}

/* getSourceCityId
 * Takes city name as input and validate it's availability in bus GDS and returns an id of passed city.
 * @param  {String} sourceCityName Ex: Bangalore
 * @return {String} An ID of source city Ex: 3
 */
LocationService.prototype.getSourceCityId = function(sourceCityName) {
    var defer = q.defer();
    var result = -1;
    // console.log('In getSourceCityId' + sourceCityName);
    fs.readFile('./app/services/bus/mockdata/sourcecitylist.json', 'utf8', function(err, data) {
        if (err) {
            console.log(err);
            defer.reject(err);
        }
            // console.log(data);

        var json = JSON.parse(data);
        json.forEach(function(obj) {
            if (obj.name.toUpperCase() == sourceCityName.toUpperCase()) {
                result = obj.id;
            }
        });
        // console.log(result);
        defer.resolve(result);
    });
    return defer.promise;
}

/* getCityId
 * Takes city name as input and validate it's availability in bus GDS and returns an id of passed city.
 * @param  {String} cityName Ex: Bangalore
 * @return {String} An ID of source city Ex: 3
 */
LocationService.prototype.getCityId = function(cityName) {
    var defer = q.defer();
    fs.readFile('./app/services/bus/mockdata/allcitieslist.json', 'utf8', function(err, data) {
        if (err) {
            console.log(err);
            defer.reject(err);
        }
        var json = JSON.parse(data);
        var result = -1;
        json.forEach(function(obj) {
            // console.log(obj.id);
            // console.log(obj.name);
            if (obj.name.toUpperCase() == cityName.toUpperCase()) {
                result = obj.id;
            }
        });
        defer.resolve(result);
    });
    return defer.promise;
}

/* isSourceDestinationPairValid
 * Takes sourceDetails and destinationDetails and checks in bus GDS whether the route is valid or not.
 * @param  {String} sourceCityName Ex: "chennai"
 * @param  {String} destinationCityName Ex: "bangalore"
 * @return {Boolean} true or false
 */
LocationService.prototype.isSourceDestinationPairValid = function(sourceCityName, destinationCityName) {
    var defer = q.defer();
    var getValue = this.getSourceCityId(sourceCityName).then(function(srcCityId) {
        fs.readFile('./app/services/bus/mockdata/sourcedestinationpairlist.json', 'utf8', function(err, data) {
            if (err) {
                console.log(err);
                defer.reject(err);
            }
            var json = JSON.parse(data);
            var res = false;
            var getCityList = json[srcCityId];
            if (getCityList != undefined) {
                getCityList.forEach(function(obj) {
                    if (obj.name.toUpperCase() == destinationCityName.toUpperCase()) {
                        res = true;
                    }
                });
            }
            defer.resolve(res);
        });
    }).catch(function(err) {
        console.log("err in calling function " + err);
    });

    return defer.promise;
}

module.exports = LocationService;
