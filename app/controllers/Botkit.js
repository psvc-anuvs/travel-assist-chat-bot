var express = require('express'),
    router = express.Router(),
    Botkit = require('botkit'),
    config = require('../../config/Config'),
    db = require('../../config/DB')({ mongoUri: config.db }),
    FBUtils = require('../utils/FBUtils'),
    pbConstants = require('../constants/PbConstants'),
    msgConstants = require('../constants/MsgConstants'),
    glob = require('glob'),
    winston = require('winston'),
    fs = require('fs');

module.exports = function(app) {

    var controller = Botkit.facebookbot({
        logger: new winston.Logger({
            levels: winston.config.syslog.levels,
            transports: [
                new(winston.transports.Console)(),
                new(winston.transports.File)({ filename: 'bot.log' })
            ]
        }),
        debug: true,
        access_token: process.env.FACEBOOK_PAGE_TOKEN,
        verify_token: process.env.FACEBOOK_VERIFY_TOKEN,
        storage: db
    });

    var bot = controller.spawn({});
    var fbutils = new FBUtils();

    //Temp function to insert city list into DB
    //TODO: out of 4020 cities only 4017 is being inserted we have check it why ?
    function addCitiesToDB() {
        fs.readFile('./app/dao/citydata/citylist.json', 'utf8', function(err, data) {
            // console.log(data);
            if (err) {
                console.log(err);
            } else {
                controller.storage.buscities.removeAll();
                var jsonData = JSON.parse(data);
                var city;
                for (var prop in jsonData) {
                    if (jsonData.hasOwnProperty(prop)) {
                        city = {                                               
                            name: jsonData[prop]
                        }
                        controller.storage.buscities.insert(city);                        
                    }
                }                
            }
        });        
    }
    

    // subscribe to page events
    var pageEventCb = function(err, body) {
        if (err) {
            controller.log('Could not subscribe to page messages');
        } else {
            controller.log('Successfully subscribed to Facebook events:', body);
            controller.log('Botkit activated');
            // start ticking to send conversation messages
            controller.startTicking();
        }
    };

    fbutils.subscribePage(pageEventCb);


    var successOrErrorCb = function(err, body) {
        if (err) {
            controller.log('There is an error', err);
        } else {
            controller.log(body);
        }
    };

    //whitelist domain
    fbutils.setWhiteList(successOrErrorCb);

    //greeting message
    fbutils.setGreetingText(successOrErrorCb);

    //setGettingStartedPayLoad
    fbutils.setGettingStartedPayLoad(successOrErrorCb);

    //set Persistant Menu
    var options = {};
    options.buttons = [];
    options.buttons.push(fbutils.createPostbackButton(msgConstants.MSG_MENU_1, pbConstants.PB_MENU_1));
    options.buttons.push(fbutils.createPostbackButton(msgConstants.MSG_MENU_2, pbConstants.PB_MENU_2));
    options.buttons.push(fbutils.createPostbackButton(msgConstants.MSG_MENU_3, pbConstants.PB_MENU_3));
    options.buttons.push(fbutils.createPostbackButton(msgConstants.MSG_MENU_4, pbConstants.PB_MENU_4));
    fbutils.setPersistentMenu(options);


    //Load chat bot scripts
    var scripts = glob.sync(config.root + '/app/skills/*.js');
    scripts.forEach(function(script) {
        require(script)(controller, bot, app);
    });

    //TODO: remove this before pushing into production
    addCitiesToDB();

    // this function processes the POST request to the webhook
    var facebook_handler = function(obj) {
        //An example of accessing global variables using app object
        controller.debug('GOT A MESSAGE HOOK')
        var message
        if (obj.entry) {
            for (var e = 0; e < obj.entry.length; e++) {
                for (var m = 0; m < obj.entry[e].messaging.length; m++) {
                    var facebook_message = obj.entry[e].messaging[m]

                    controller.log(facebook_message);
                    // normal message
                    if (facebook_message.message) {
                        message = {
                            text: facebook_message.message.text,
                            user: facebook_message.sender.id,
                            channel: facebook_message.sender.id,
                            timestamp: facebook_message.timestamp,
                            seq: facebook_message.message.seq,
                            mid: facebook_message.message.mid,
                            attachments: facebook_message.message.attachments
                        }

                        // // save if user comes from m.me adress or Facebook search                        
                        create_user_if_new(facebook_message.sender.id, facebook_message.timestamp)

                        controller.receiveMessage(bot, message)
                    }
                    // clicks on a postback action in an attachment
                    else if (facebook_message.postback) {
                        // trigger BOTH a facebook_postback event
                        // and a normal message received event.
                        // this allows developers to receive postbacks as part of a conversation.
                        message = {
                            payload: facebook_message.postback.payload,
                            user: facebook_message.sender.id,
                            channel: facebook_message.sender.id,
                            timestamp: facebook_message.timestamp
                        }

                        controller.trigger('facebook_postback', [bot, message])

                        message = {
                            text: facebook_message.postback.payload,
                            user: facebook_message.sender.id,
                            channel: facebook_message.sender.id,
                            timestamp: facebook_message.timestamp
                        }

                        controller.receiveMessage(bot, message)
                    }
                    // When a user clicks on "Send to Messenger"
                    else if (facebook_message.optin) {
                        message = {
                            optin: facebook_message.optin,
                            user: facebook_message.sender.id,
                            channel: facebook_message.sender.id,
                            timestamp: facebook_message.timestamp
                        }

                        //TODO: save if user comes from "Send to Messenger"
                        create_user_if_new(facebook_message.sender.id, facebook_message.timestamp)

                        controller.trigger('facebook_optin', [bot, message])
                    }
                    // message delivered callback
                    else if (facebook_message.delivery) {
                        message = {
                            optin: facebook_message.delivery,
                            user: facebook_message.sender.id,
                            channel: facebook_message.sender.id,
                            timestamp: facebook_message.timestamp
                        }

                        controller.trigger('message_delivered', [bot, message])
                    } else {
                        controller.log('Got an unexpected message from Facebook: ', facebook_message)
                    }
                }
            }
        }
    };

    var create_user_if_new = function(id, ts) {
        controller.storage.users.get(id)
            .then(function(user) {
                if (!user) {
                    var options = {};
                    options.userId = id;
                    options.ts = ts;
                    fbutils.getUserProfile(options)
                        .then(function(userProfile) {
                            controller.storage.users.save(userProfile);
                        })
                        .catch(function(err) {
                            controller.log('Error in creating user profile ', err);
                        });
                }
            })
            .catch(function(err) {
                controller.log('Error in creating user profile ', err);
            });
    }

    // ROUTES
    app.route('/webhook')

    .get(function(req, res) {
        // This enables subscription to the webhooks
        if (req.query['hub.mode'] === 'subscribe' && req.query['hub.verify_token'] === process.env.FACEBOOK_VERIFY_TOKEN) {
            res.send(req.query['hub.challenge'])
        } else {
            res.send('Incorrect verify token')
        }
    })

    .post(function(req, res) {
        facebook_handler(req.body)
        res.send('ok')
    });
};
