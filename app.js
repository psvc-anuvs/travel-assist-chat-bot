var express = require('express'),
    config = require('./config/Config'),
    glob = require('glob'),
    dotenv = require('dotenv');


dotenv.load();

var app = express();

require('./config/Express')(app, config);

app.listen(config.port, function() {
    console.log('Express server listening on port ' + config.port);
});
