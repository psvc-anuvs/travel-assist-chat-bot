var db = require('monk'),
    BaseDao = require('../app/dao/BaseDao'),
    UserDao = require('../app/dao/UserDao');

/* botkit-storage-mongo - MongoDB driver for Botkit
 *
 * @param  {Object} config
 * @return {Object}
 */
module.exports = function(config) {
    if (!config || !config.mongoUri) {
        throw new Error('Need to provide mongo address.')
    }

    var Teams = db(config.mongoUri).get('teams');
    var Users = db(config.mongoUri).get('users');
    var Channels = db(config.mongoUri).get('channels');
    var BusBookings = db(config.mongoUri).get('busbookings');
    var BusCities = db(config.mongoUri).get('buscities');

    return {
        teams: new BaseDao(Teams),
        users: new UserDao(Users),
        channels: new BaseDao(Channels),
        busbookings: new BaseDao(BusBookings),
        buscities: new BaseDao(BusCities)
    }
}
